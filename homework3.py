import os, re, csv

def search():
    for adress, dirs, files in os.walk(r'C:\Users\valer\Desktop\logs'):
        for file in files:
            if file.endswith('.log') or file.endswith('.txt'):
                yield os.path.join(adress, file)

regexp = '.*' + input('Что искать?: ') + '.*'

def read_from_pathtxt(path):
    with open(path) as r:
        log = r.read()
        log_list = re.findall(regexp, log)
        file = open('output.txt', 'w')
        for line in log_list:
            output = path + ' ' + line + '\n'
            print(output)
            file.write(output)
            
for i in search():
    read_from_pathtxt(i)
